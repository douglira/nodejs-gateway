const express = require('express')
const router = express.Router()
const requireDir = require('require-dir')

const {
  AutenticacaoController
} = requireDir('./app/controllers')
const {
  Error: ErrorMiddleware,
  Autorizacao: AutorizacaoMiddleware,
  ProxyClinCore,
  ProxyClinAgenda
} = requireDir('./app/middlewares')
const RouteValidators = require('./app/validators/RouteValidators')

/**
 * Autenticacao
 */
router.post('/login', express.json(), RouteValidators.login, AutenticacaoController.login)

/**
  * Middleware de autorização à API
  */
router.use(AutorizacaoMiddleware)

/**
 * Proxy clin-core
 */
router.use(ProxyClinCore)
/**
 * Proxy clin-agenda
 */
router.use(ProxyClinAgenda)

/**
 * Middleware para capturar e tratar erros ou exceções na API
 */
router.use(ErrorMiddleware)

module.exports = router
