const express = require('express')
const cors = require('cors')
const helmet = require('helmet')
const morgan = require('morgan')

class App {
  constructor () {
    this.express = express()
    this.isDev = process.env.NODE_ENV !== 'production'

    this.middlewares()
    this.routes()
  }

  middlewares () {
    this.express.use(helmet())
    this.express.use(morgan())
    this.express.use(cors())
  }

  routes () {
    this.express.use(require('./routes'))
  }
}

module.exports = new App().express
