module.exports = {
  upperCaseConverter (key) {
    return function (value) {
      if (String(value).trim()) {
        this.setDataValue(key, String(value).toUpperCase())
      }
    }
  },
  lowerCaseConverter (key) {
    return function (value) {
      if (String(value).trim()) {
        this.setDataValue(key, String(value).toLowerCase())
      }
    }
  },
  apenasDigitosConverter (key) {
    return function (value) {
      if (String(value).trim()) {
        this.setDataValue(key, String(value).replace(/[^\d]/g, ''))
      }
    }
  },
  bitBoolean (key) {
    return function (value) {
      if (typeof value === 'boolean' && value) {
        this.setDataValue(key, 1)
      }

      if (typeof value === 'boolean' && !value) {
        this.setDataValue(key, 0)
      }

      this.setDataValue(key, value)
    }
  }
}
