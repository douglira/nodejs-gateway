const winston = require('winston')
const logger = winston.createLogger({
  level: 'error',
  format: winston.format.json()
})

module.exports = (err, req, res, next) => {
  if (process.env.NODE_ENV !== 'production') {
    logger.error(err)
  }

  return res.status(500).json({ err: 'Erro inesperado. Por favor tente novamente' })
}
