const proxy = require('http-proxy-middleware')
// const winston = require('winston')

// const logProvider = provider => {
//   const logger = winston.createLogger({
//     format: winston.format.json()
//   })

//   return {
//     log: logger.log,
//     debug: logger.debug,
//     info: logger.info,
//     warn: logger.warn,
//     error: logger.error
//   }
// }

module.exports = proxy('/clin-core/', {
  target: 'http://127.0.0.1:7300',
  changeOrigin: true,
  pathRewrite: (path, req) => path.replace('/clin-core', '/')
  // logProvider
})
