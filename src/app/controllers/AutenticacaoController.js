const usuarioCore = require('../core/UsuarioCore')

class AutenticacaoController {
  async login (req, res, next) {
    try {
      const { username, password } = req.body
      const usuario = usuarioCore.autenticar(username, password)

      res.json(usuario)
    } catch (err) {
      next(err)
    }
  }
}

module.exports = new AutenticacaoController()
