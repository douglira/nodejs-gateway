const { Usuario } = require('../models')

class UsuarioCore {
  async autenticar (email, senha) {
    const usuario = await Usuario.findOne({ where: { email } })

    if (!usuario) {
      throw new Error('Usuario ou senha inválidos')
    }

    if (!await usuario.verificarSenha(senha)) {
      throw new Error('Usuario ou senha inválidos')
    }
  }
}

module.exports = new UsuarioCore()
