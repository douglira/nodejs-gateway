const { checkSchema } = require('express-validator/check')

class RouteValidators {
  static login () {
    return checkSchema({
      username: {
        in: ['body'],
        trim: true,
        errorMessage: 'Login obrigatório',
        isLength: {
          errorMessage: 'Mínimo 5 caracteres',
          options: { min: 5 }
        }
      },
      password: {
        in: ['body'],
        trim: true,
        errorMessage: 'Senha obrigatória',
        isLength: {
          errorMessage: 'Mínimo 4 caracteres',
          options: { min: 4 }
        }
      }
    })
  }
}

module.exports = RouteValidators
