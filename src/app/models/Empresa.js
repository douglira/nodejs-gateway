module.exports = (sequelize, DataTypes) => {
  const Empresa = sequelize.define('Empresa', {
    nome: DataTypes.STRING,
    identificador: DataTypes.STRING
  }, {
    tableName: 'empresa'
  })

  Empresa.associate = (models) => {
    Empresa.belongsTo(models.Usuario, { as: 'modificadorPor', foreignKey: 'modificado_por' })
    Empresa.hasMany(models.EmpresaUnidade, { as: 'unidades', foreignKey: 'empresa_id' })
  }

  return Empresa
}
