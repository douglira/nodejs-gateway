module.exports = (sequelize, DataTypes) => {
  const UsuarioEmpresaUnidade = sequelize.define('UsuarioEmpresaUnidade', {
    usuario_id: DataTypes.INTEGER,
    empresa_unidade_id: DataTypes.INTEGER
  }, {
    tableName: 'usuario_empresa_unidade'
  })

  return UsuarioEmpresaUnidade
}
