module.exports = (sequelize, DataTypes) => {
  const Pais = sequelize.define('Pais', {
    nome: DataTypes.STRING,
    codigoBacen: DataTypes.STRING
  }, {
    tableName: 'pais'
  })

  return Pais
}
