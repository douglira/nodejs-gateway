module.exports = (sequelize, DataTypes) => {
  const Estado = sequelize.define('Estado', {
    uf: {
      type: DataTypes.STRING,
      primaryKey: true,
      allowNull: false
    },
    nome: DataTypes.STRING
  }, {
    tableName: 'estado'
  })

  return Estado
}
