const { upperCaseConverter, apenasDigitosConverter, lowerCaseConverter, bitBoolean } = require('../../libs/setters')

module.exports = (sequelize, DataTypes) => {
  const EmpresaUnidade = sequelize.define('EmpresaUnidade', {
    razaoSocial: {
      type: DataTypes.STRING,
      set: upperCaseConverter('razaoSocial')
    },
    nome: {
      type: DataTypes.STRING,
      set: upperCaseConverter('razaoSocial')
    },
    cnpj: {
      type: DataTypes.STRING,
      set: apenasDigitosConverter('cnpj')
    },
    endereco: {
      type: DataTypes.STRING,
      set: upperCaseConverter('endereco')
    },
    enderecoBairro: {
      type: DataTypes.STRING,
      set: upperCaseConverter('enderecoBairro')
    },
    enderecoNumero: {
      type: DataTypes.STRING,
      set: upperCaseConverter('enderecoNumero')
    },
    enderecoComplemento: {
      type: DataTypes.STRING,
      set: upperCaseConverter('enderecoComplemento')
    },
    cep: {
      type: DataTypes.STRING,
      set: apenasDigitosConverter('cep')
    },
    email: {
      type: DataTypes.STRING,
      set: lowerCaseConverter('email')
    },
    telefone: {
      type: DataTypes.STRING,
      set: apenasDigitosConverter('telefone')
    },
    celular: {
      type: DataTypes.STRING,
      set: apenasDigitosConverter('celular')
    },
    ativo: {
      type: DataTypes.CHAR(1),
      defaultValue: 1,
      set: bitBoolean('medico')
    },
    identificador: {
      type: DataTypes.STRING,
      set: lowerCaseConverter('identificador')
    }
  }, {
    tableName: 'empresa_unidade',
    defaultScope: {
      include: [
        { model: sequelize.models.Cidade, as: 'cidade' }
      ]
    }
  })

  EmpresaUnidade.associate = models => {
    EmpresaUnidade.belongsTo(models.Usuario, { as: 'modificadorPor', foreignKey: 'modificado_por' })
    EmpresaUnidade.belongsTo(models.Empresa, { as: 'empresa', foreignKey: 'empresa_id' })
    EmpresaUnidade.belongsTo(models.Cidade, { as: 'cidade', foreignKey: 'cidade_id' })
  }

  return EmpresaUnidade
}
