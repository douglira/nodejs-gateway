module.exports = (sequelize, DataTypes) => {
  const Cidade = sequelize.define('Cidade', {
    nome: DataTypes.STRING,
    codigoIbge: DataTypes.INTEGER
  }, {
    tableName: 'cidade'
  })

  Cidade.associate = models => {
    Cidade.belongsTo(models.Estado, { as: 'estado', foreignKey: 'codigo_ibge' })
    Cidade.belongsTo(models.Pais, { as: 'pais', foreignKey: 'codigo_ibge' })
  }

  return Cidade
}
