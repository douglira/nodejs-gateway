module.exports = (sequelize, DataTypes) => {
  const Permissao = sequelize.define('Permissao', {
    nome: DataTypes.STRING,
    descricao: DataTypes.STRING
  }, {
    tableName: 'permissao'
  })

  Permissao.associate = (models) => {
    Permissao.belongsTo(models.Usuario, { as: 'modificadorPor', foreignKey: 'modificado_por' })
  }

  return Permissao
}
