const bcrypt = require('bcryptjs')
const { bitBoolean } = require('../../libs/setters')

module.exports = (sequelize, DataTypes) => {
  const Usuario = sequelize.define('Usuario', {
    nome: DataTypes.STRING,
    email: DataTypes.STRING,
    senha: DataTypes.STRING,
    novaSenha: DataTypes.VIRTUAL,
    medico: {
      type: DataTypes.CHAR(1),
      defaultValue: 1,
      set: bitBoolean('medico')
    }
  }, {
    tableName: 'usuario',
    defaultScope: {
      include: [
        { model: sequelize.models.Permissao, as: 'permissoes' },
        { model: sequelize.models.EmpresaUnidade, as: 'unidades' }
      ]
    },
    hooks: {
      beforeSave: async usuario => {
        if (usuario.novaSenha) {
          usuario.senha = await bcrypt.hash(usuario.novaSenha, 10)
        }
      }
    }
  })

  Usuario.associate = (models) => {
    Usuario.belongsTo(models.Usuario, { as: 'modificadorPor', foreignKey: 'modificado_por' })
    Usuario.belongsToMany(models.Permissao, { as: 'permissoes', through: models.UsuarioPermissao, foreignKey: 'usuario_id', otherKey: 'permissao_id' })
    Usuario.belongsToMany(models.EmpresaUnidade, { as: 'unidades', through: models.UsuarioEmpresaUnidade, foreignKey: 'usuario_id', otherKey: 'empresa_unidade_id' })
  }

  Usuario.prototype.verificarSenha = function (senha) {
    return bcrypt.compare(this.senha, senha)
  }

  return Usuario
}
