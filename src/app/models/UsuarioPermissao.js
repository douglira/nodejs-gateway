module.exports = (sequelize, DataTypes) => {
  const UsuarioPermissao = sequelize.define('UsuarioPermissao', {
    usuario_id: DataTypes.INTEGER,
    permissao_id: DataTypes.INTEGER
  }, {
    tableName: 'usuario_permissao'
  })

  return UsuarioPermissao
}
