## Configuração e instruções

Antes de rodar a aplicação, abra o terminal e rode o comando **npm install** para baixar as dependências do projeto. Feito isto, configure o arquivo **.env.development (para desenvolvimento)** e o **.env.production (para produção)**. Nele é configurado a variáveis de ambiente relacionado ao banco de dados, nodejs, etc...

Para executar o projeto em desenvolvimento, rode o comando **npm run dev** no terminal na raiz do projeto
